package com.vazhasapp.assignment20

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.vazhasapp.assignment20.adapters.MatchAdapter
import com.vazhasapp.assignment20.databinding.ActivityMainBinding
import com.vazhasapp.assignment20.models.Match

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val myAdapter = MatchAdapter()
    private val dummyList = mutableListOf<Match>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
    }

    private fun init() {
        setupRecycler()
        myAdapter.setData(fillDummyData())
    }

    private fun fillDummyData(): MutableList<Match> {
        repeat(10) {
            dummyList.add(Match("Dinamo Vs Goris Shuadghe", "Axmeta"))
        }
        return dummyList
    }

    private fun setupRecycler() {
        binding.recyclerView.apply {
            adapter = myAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
            setHasFixedSize(true)
        }
    }
}