package com.vazhasapp.assignment20.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding

typealias RecyclerLayoutInflater<T> = (LayoutInflater, ViewGroup?, Boolean) -> T

abstract class BaseAdapter<T : Any, VB : ViewBinding>(
    private val layoutInflater: RecyclerLayoutInflater<VB>
) : RecyclerView.Adapter<BaseAdapter.BaseViewHolder<VB>>() {

    protected val itemList = mutableListOf<T>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        BaseViewHolder(
            layoutInflater.invoke(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun getItemCount() = itemList.size

    class BaseViewHolder<VB : ViewBinding>(val binding: VB) :
        RecyclerView.ViewHolder(binding.root)

    fun setData(itemList: MutableList<T>) {
        this.itemList.clear()
        this.itemList.addAll(itemList)
        notifyDataSetChanged()
    }
}