package com.vazhasapp.assignment20.adapters

import com.vazhasapp.assignment20.databinding.MatchLayoutBinding
import com.vazhasapp.assignment20.models.Match

class MatchAdapter : BaseAdapter<Match, MatchLayoutBinding>(MatchLayoutBinding::inflate) {

    override fun onBindViewHolder(
        holder: BaseViewHolder<MatchLayoutBinding>,
        position: Int
    ) {
        holder.binding.tvMatchTitle.text = itemList[position].name
        holder.binding.tvAddress.text = itemList[position].address
    }
}