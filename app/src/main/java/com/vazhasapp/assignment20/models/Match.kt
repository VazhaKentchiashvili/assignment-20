package com.vazhasapp.assignment20.models

data class Match(
    val name: String,
    val address: String,
)
